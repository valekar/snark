from ModelTrails.generator import Generator
from keras.callbacks import CSVLogger
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from ModelTrails.model import MyModel
from ModelTrails.datapreprocessor import DataPreprocessor
import os
from keras.models import load_model
from keras import backend as K
import tensorflow as tf
from tensorflow.python.client import timeline

#import matplotlib.pyplot as plt
from keras.callbacks import Callback
from keras.optimizers import Adam


class LRFinder(Callback):
    '''
    This callback implements a learning rate finder(LRF)
    The learning rate is constantly increased during training.
    On training end, the training loss is plotted against the learning rate.
    One may choose a learning rate for a model based on the given graph,
    selecting a value slightly before the minimal training loss.
    The idea was introduced by Leslie N. Smith in this paper: https://arxiv.org/abs/1506.01186
    # Example
        lrf = LRFinder(max_iterations=5000, base_lr = 0.0001, max_lr = 0.1)
        model.fit(x_train, y_train, epochs=1, batch_size=128, callbacks=[LRF])
    # Arguments
        max_iterations: training stops when max_iterations are reached
        base_lr: initial learning rate used in training
        max_lr: training stops when learning rate exceeds max_lr
        lr_step_size: for each batch, the learning rate is increased by
            lr_step_size
    '''

    def __init__(self, max_iterations=5000, base_lr=0.0001, max_lr=0.1, lr_step_size=0.0001):
        self.max_iterations = max_iterations
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.lr_step_size = lr_step_size
        self.losses = []
        self.lrs = []
        self.lr = 0

    def on_batch_end(self, batch, logs={}):
        iterations = logs.get('batch')
        if (iterations >= self.max_iterations or self.lr >= self.max_lr):
            self.model.stop_training = True
        self.lr = self.base_lr + iterations * self.lr_step_size
        K.set_value(self.model.optimizer.lr, self.lr)
        self.losses.append(logs.get('loss'))
        self.lrs.append(self.lr)

    def on_train_end(self, logs=None):
        plt.plot(self.lrs, self.losses)
        plt.show()


def train_my_model(model, train_save_arg, epochs=1):
    batch_sz = 128
    processed_data_path = os.path.join("data", "processed")

    generator = Generator(data_path=processed_data_path,
                          training_filename='caption.txt',
                          train_image_features_name='inception_image_to_features.h5',
                          val_image_features='inception_image_to_features.h5',
                          batch_size=batch_sz)

    num_training_samples = generator.training_dataset.shape[0]
    num_validation_samples = generator.validation_dataset.shape[0]
    print('Number of training samples:', num_training_samples)
    print('Number of validation samples:', num_validation_samples)

    training_hist_filename = os.path.join("data", "trained_models", "flickr",
                                          "training_history_.0{0}.log".format(train_save_arg))
    csv_logger = CSVLogger(training_hist_filename, append=False)

    model_names = (os.path.join("data", "trained_models", "flickr", 'flickr_weights.{epoch:02d}-{val_loss:.2f}.hdf5'))
    model_checkpoint = ModelCheckpoint(model_names, monitor='val_loss', verbose=1, save_best_only=True,
                                       save_weights_only=False)

    reduce_learning_rate = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, verbose=1)
    csv_callbacks = [csv_logger, model_checkpoint, reduce_learning_rate]

    training_history = model.fit_generator(generator=generator.train_flow(),
                                           steps_per_epoch=int(num_training_samples / batch_sz),
                                           epochs=epochs,
                                           verbose=1,
                                           callbacks=csv_callbacks,
                                           validation_data=generator.validation_flow(),
                                           validation_steps=int(num_validation_samples / batch_sz))

    val_loss = training_history.history['val_loss']
    print("validation lossss::::", val_loss)


def load_my_model(mode='new', saved_filename=None):
    # model = None
    if mode == 'existing':
        print("Loading the existing model ....", saved_filename)
        model_filename = (os.path.join('data', 'trained_models', saved_filename))
        model = load_model(model_filename)
    else:
        print("Creating a new model ....")
        model_class = MyModel(max_token_len=22, vocab_size=5744)
        model = model_class.captioner(rnn='lstm', no_img_feats=2048,
                                      hidden_size=128, emb_size=128, regularizer=1e-8)

        opti = Adam(lr= 0.09, beta_1=0.9, beta_2=0.999, epsilon=10e-8, decay=0.0, amsgrad=False)
		#opti = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=10e-8, decay=0.0, amsgrad=False)
        model.compile(loss='categorical_crossentropy',
                      optimizer=opti,
                      metrics=['accuracy'])

    print(model.summary())
    print('Number of parameters:', model.count_params())
    return model


'''def customized_training():
    epochs = 15
    for epoch in range(1, epochs):
        K.clear_session()
        with K.get_session() as s:
            run_options = \
                tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()

            train_my_model(model=load_my_model(mode='existing',
                           saved_filename='mscoco_weights.1.hdf5'.format(epoch)),
                           train_save_arg=epoch)

            to = timeline.Timeline(run_metadata.step_stats)
            trace = to.generate_chrome_trace_format()
            with open(os.path.join('data', 'log', 'memory_trace.json'),
                      'w') as out:
                out.write(trace)
    return None'''


def normal_training(train_save_arg=0):
    epochs = 50
    model = load_my_model(mode='new', saved_filename=None)
    train_my_model(model=model, train_save_arg=train_save_arg, epochs=epochs)
    return None


def existing_training(train_save_arg=1):
    epochs = 950
    model = load_my_model(mode='existing', saved_filename='flickr\\flickr_weights.50-1.21.hdf5')
    train_my_model(model=model, train_save_arg=train_save_arg, epochs=epochs)


if __name__ == "__main__":
    normal_training(0)
    #existing_training(1)
